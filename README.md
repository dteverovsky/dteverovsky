# Hi I'm Danika :wave:

Senior UX Researcher at GitLab supporting the [Plan](https://handbook.gitlab.com/handbook/engineering/development/dev/plan/) stage.

Boston, MA, US is home, with my partner and two cats :cat:. I love to travel and get into nature 🥾 :mountain:. If I'm not at home, you can probably find me at my local climbing gym!

Set up a coffee chat with me if you'd like to chat ☕ or learn more about [UX Research](https://handbook.gitlab.com/handbook/product/ux/ux-research/) at GitLab.

Quick Links:
- [My research priority issue](https://gitlab.com/gitlab-org/ux-research/-/issues/3339)
- [Plan Stage UX Research Wiki](https://gitlab.com/gitlab-org/plan-stage/plan-ux-insights/-/wikis/Home)
- [Plan Stage Actionable Insights](https://gitlab.com/groups/gitlab-org/-/issues/?sort=updated_desc&state=opened&label_name%5B%5D=devops%3A%3Aplan&or%5Blabel_name%5D%5B%5D=Actionable%20Insight%3A%3AExploration%20needed&or%5Blabel_name%5D%5B%5D=Actionable%20Insight%3A%3AProduct%20change&first_page_size=100)